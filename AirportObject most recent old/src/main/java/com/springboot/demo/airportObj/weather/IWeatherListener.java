package com.springboot.demo.airportObj.weather;

import java.util.List;

import com.springboot.demo.airportObj.weather.datasource.METAR_Data;
import com.springboot.demo.airportObj.weather.datasource.TAFS_Data;
import com.springboot.demo.airportObj.weather.datasource.WeatherData;

public interface IWeatherListener {
	
	public void newWeatherComing ( METAR_Data metar ) ; 
	public void newWeatherComing ( TAFS_Data metar ) ; 
	public void newWeatherComing ( List<List<WeatherData>> weatherDataList ) ;
}
