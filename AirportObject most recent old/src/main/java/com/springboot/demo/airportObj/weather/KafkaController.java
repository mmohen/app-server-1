package com.springboot.demo.airportObj.weather;

import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.springboot.demo.airportObj.weather.datasource.METAR_Data;
import com.springboot.demo.airportObj.weather.datasource.TAFS_Data;
import com.springboot.demo.airportObj.weather.datasource.WeatherData;

@Controller
public class KafkaController {

	@Autowired
	WeatherBLL wbll ;

	@Autowired 
	private IMetarRepo metarRepo ; 

	String data = "None" ;
	List<List<WeatherData>> weatherList ;

	@PostConstruct
	public void setupListener () { 

		wbll.setWeatherListener( new IWeatherListener() {

			@Override
			public void newWeatherComing(METAR_Data metar) {

				 System.out.println("Kafka METAR: " + metar.toString() );
				 // insertMetar ( metar ) ;
			}

			@Override
			public void newWeatherComing(TAFS_Data tafs) {

				//System.out.println("Kafka TAFS: " + tafs.toString() );

			}

			@Override
			public void newWeatherComing(List<List<WeatherData>> weatherDataList) {
				
				pushToKafka (weatherDataList ) ;

			}			
		} );

	}

	private void pushToKafka (List<List<WeatherData>> weatherDataList) { 
		
		
		
		// System.out.println( weatherDataList.toString());
		
		
	}
	
	
	/*
	private void insertMetar ( METAR_Data newMetar ) { 


		System.out.println( "INSERT: " + newMetar.toString() );

		try { 
			
			metarRepo.save( newMetar ) ;
			
			
			
		}catch ( Exception e ) { 

			System.out.println( e.toString() );
		}
	}
*/


}
