package com.springboot.demo.airportObj.weather;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Dictionary;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationListener;
import org.springframework.context.PayloadApplicationEvent;
import org.springframework.core.ResolvableType;
import org.springframework.stereotype.Service;

import com.springboot.demo.airportObj.weather.datasource.MetarDoc;
import com.springboot.demo.airportObj.weather.datasource.Forecast;
import com.springboot.demo.airportObj.weather.datasource.METAR_Data;
import com.springboot.demo.airportObj.weather.datasource.Notam_Data;
import com.springboot.demo.airportObj.weather.datasource.ReportType;
import com.springboot.demo.airportObj.weather.datasource.TAFS_Data;
import com.springboot.demo.airportObj.weather.datasource.WeatherData;

@Service
public class WeatherBLL implements ApplicationListener {


	private StringBuilder matarLatestObsTime ; 
	private StringBuilder tafsLatestIssueTime ; 

	private TAFS_Data  tafLocal  ; 
	private METAR_Data metarLocal ; 		
	private List<IWeatherListener> weatherListener ;
	private HashMap <String, Notam_Data> notamDataList ; 
	
	
	SimpleDateFormat dateFormatter ;
	Format formatter ;

	@Autowired 
	private IMetarRepo metarRepo ; 
	
	@Autowired
	private ITafsRepo tafsRepo ; 
	

	public WeatherBLL() {

		super();

		matarLatestObsTime = new StringBuilder("") ; 
		tafsLatestIssueTime = new StringBuilder("") ;
		dateFormatter = new SimpleDateFormat ( "yyyy-MM-dd'T'HH:mm:ss'Z'" ) ; 
		formatter = new SimpleDateFormat ( "yyyy-MM-dd'T'HH:mm:ss'Z'" ) ; 
		weatherListener = new ArrayList<IWeatherListener>() ;
		notamDataList = new HashMap <String, Notam_Data>() ; 
	}	

	public void setWeatherListener ( IWeatherListener newWeatherListener ) { 

		this.weatherListener.add( newWeatherListener ) ;
	}	


	private synchronized List<List<WeatherData>> createWeatherForecast (ApplicationEvent data ) {

		// For NOW, baseOnTimeFrame  15, 30, 60 for ONLY one airport KATL
		System.out.println ( "---IN---") ;

		if ( data instanceof TAFS_Data ) { 

			tafLocal = (TAFS_Data) data ;
		}
		else if ( data instanceof METAR_Data ) { 

			metarLocal = (METAR_Data) data ;
		}



		List<List<WeatherData>> weatherDataList = new ArrayList<List<WeatherData>>() ;

		if ( metarLocal == null ||  tafLocal == null ) { 

			if ( metarLocal == null ) 
				System.out.println( "metarLocal null");
			if ( tafLocal == null ) 
				System.out.println( "tafLocal null");
			if ( metarLocal != null ) 
				System.out.println( "metarLocal NOT null");
			if ( tafLocal != null ) 
				System.out.println( "tafLocal NOT null");

			System.out.println ( "---OUT---") ;
			return weatherDataList ;
		}


		int timeFrame[] = { 15, 30, 60 } ; 

		try { 

			Calendar calDate = Calendar.getInstance() ;    
			calDate.setTime( dateFormatter.parse( metarLocal.getObservationTime() ) ) ; 

			if ( calDate.get(Calendar.MINUTE) > 30 ) { 

				calDate.add(Calendar.HOUR, 1);				
			}

			calDate.set(Calendar.MINUTE, 0) ;

			Date weatherStart =  calDate.getTime() ; 
			calDate.add(Calendar.DATE, 1 ); 
			Date weatherEnd = calDate.getTime() ; 

			int size = 96 + 1 ; // 24 hours of forcast plus one metar 
			WeatherData[] weatherData = new WeatherData[ size ]  ;  // every 15 min in 24 hours		

			for ( int index = 0 ; index < size ; index ++ ) { 

				weatherData[index] = new WeatherData () ;
			}

			weatherData[0].setType(ReportType.METAR);
			weatherData[0].setAirportCode(metarLocal.getAirportCode());
			weatherData[0].setTime(metarLocal.getObservationTime());
			weatherData[0].setVisibilityStatusMi(metarLocal.getVisibility());
			weatherData[0].setWindSpeetKt(metarLocal.getWindSpeetKt());
			weatherData[0].setCloudBase( metarLocal.getCloudBase()  );

			// ******************************************
		    // * Remove all documents in tafs collection *
			// ******************************************
			//
			//   Code here ....
			//
			// ******************************************
			
			// weatherStart is used for target time frame 
			for ( int index = 1 ; index < size; index++ ) { 

				calDate.setTime(weatherStart);
				calDate.add(Calendar.MINUTE, 15 );
				weatherStart = calDate.getTime() ;
				weatherData[index].setTime( formatter.format(weatherStart) );

				
				for ( Forecast forecast : tafLocal.getForecasts() ) { 

					if ( ( weatherStart.compareTo(forecast.getFromTimeDate()) == 0 || weatherStart.after(forecast.getFromTimeDate() ) )&& 
							weatherStart.before( forecast.getToTimeDate() ) ) { 

						weatherData[index].setType(ReportType.TAFS);
						weatherData[index].setAirportCode(tafLocal.getAirportCode());						
						weatherData[index].setVisibilityStatusMi( forecast.getVisibility());
						weatherData[index].setWindSpeetKt( forecast.getWindSpeetKt());
						weatherData[index].setCloudBase( forecast.getCloudBase());		
						weatherData[index].setWindDirection(forecast.getWindDirection());
						weatherData[index].setChangeIndicator(forecast.getChangeIndicator());
						weatherData[index].setIssueTime(tafLocal.getIssueTime());
												
					     tafsRepo.save ( weatherData[index] ) ;
					}		    		 
				}   	
			}


		
		   	 
			weatherDataList.add( Arrays.asList(weatherData)) ;

			for ( int index = 1 ; index < timeFrame.length ; index++ ) { 

				List<WeatherData> newList = new ArrayList<>() ; 

				for ( int target = timeFrame[index]/15  ; target < size ; target+= timeFrame[index]/15 ) { 

					WeatherData newData = new WeatherData() ; 

					newData.setType(ReportType.TAFS);
					newData.setAirportCode( weatherData[target].getAirportCode());
					newData.setTime( weatherData[target].getTime() );
					newData.setVisibilityStatusMi( weatherData[target].getVisibilityStatusMi());
					newData.setWindSpeetKt( weatherData[target].getWindSpeetKt());
					newData.setCloudBase( weatherData[target].getCloudBase());
					newData.setWindDirection(weatherData[target].getWindDirection());

					newList.add(newData) ; 
					
				}

				newList.add(0, weatherData[0]);
				weatherDataList.add(newList) ;

			}

		} catch ( ParseException e ) { 

			System.out.println( "Exception: " + e ); 
			return null ;
		}


		System.out.println ( "---OUT END---") ;
		return weatherDataList ; 

	}



	@Override
	public synchronized void onApplicationEvent(ApplicationEvent data) {

		if ( data instanceof TAFS_Data ) { 							

			if ( tafsLatestIssueTime.length() == 0 || !tafsLatestIssueTime.toString().equals( ((TAFS_Data) data).getIssueTime() ) ) { 

				System.out.println("Received TAFS:" );
				System.out.println(tafsLatestIssueTime.length());
				System.out.println(tafsLatestIssueTime );
				System.out.println(((TAFS_Data) data).getIssueTime() );

				tafsLatestIssueTime.setLength(0);  
				tafsLatestIssueTime.append(((TAFS_Data) data).getIssueTime() );	
				//tafLocal = (TAFS_Data) data ;

				List<List<WeatherData>>  weatherData = createWeatherForecast( data ) ; 

				for ( IWeatherListener listener : weatherListener ) { 

					listener.newWeatherComing( weatherData ) ;
				}
			}
		}

		if ( data instanceof METAR_Data ) { 

			data = (METAR_Data) data ;		

			if ( matarLatestObsTime.length() == 0 || !matarLatestObsTime.toString().equals( ((METAR_Data) data).getObservationTime())) {

				System.out.println("BLL Received METAR - " + ((METAR_Data)data).toString() );

				// metarLocal = (METAR_Data) data ;
				matarLatestObsTime.setLength(0);
				matarLatestObsTime.append(((METAR_Data) data).getObservationTime()) ;

				List<List<WeatherData>>  weatherData = createWeatherForecast( data ) ; 

				insertMetar() ;
				
				for ( IWeatherListener listener : weatherListener ) { 

					listener.newWeatherComing ( weatherData ) ;
				 //listener.newWeatherComing(metarLocal);
				}

			}
		}
		
		
		if ( data instanceof PayloadApplicationEvent  ) { 
			
			Object obj = data.getSource() ; 
			
			if ( obj.getClass().isInstance(Notam_Data.class) ) {
			
		         List<Notam_Data> list = (List<Notam_Data>) ((PayloadApplicationEvent) data).getPayload()   ; 
		   
		         System.out.println ( "-----Starts---------" ) ; 
		         
		         for ( Notam_Data notam : list ) { 
		        	 
		        	 String expTime = notam.getBody().getData().getNotam_expire_time() ; 
		        	 String report =  notam.getBody().getData().getNotam_report() ;
		        	 
		        	 report = report.substring(0, report.indexOf(".") ) ; 
		        	 System.out.println ( expTime +  " "  +  report ) ; 
		        	 
		         }
		         
		         System.out.println ( "-----End--------" ) ; 
			}		
		}
		
		
	/*
		
		if ( data instanceof List<Notam_Data> ) { 
			
			Notam_Data notam = (Notam_Data) data ; 
			
			if ( ! notamDataList.containsKey( notam.get_id() ) ) { 
				
				notamDataList.put(notam.get_id(), notam) ;	
								
			}	
		}
		*/
		
	}

    
	private void insertMetar ( ) { 


		System.out.println( "INSERT: " + metarLocal.toString() );

		try { 

			     metarRepo.save( metarLocal.createMetarDoc() ) ;

		}catch ( Exception e ) { 

			System.out.println( "Metar insert Exception "+  e.toString() );
		}
	}


}
