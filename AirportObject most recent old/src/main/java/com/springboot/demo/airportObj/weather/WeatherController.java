package com.springboot.demo.airportObj.weather;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.springboot.demo.airportObj.weather.datasource.WeatherData;
import com.weather.xml.metar.Data;
import com.weather.xml.metar.METAR;
import com.weather.xml.metar.Response;
import com.weather.xml.metar.SkyCondition;

@RestController
public class WeatherController {
	
	@Autowired 
	SocketReqController socketCont ;
	
	
	@RequestMapping("/hello")
	public String sayHi () { 
				
		return "HI" ;
	}
	
	@RequestMapping("/metar")
	@ResponseBody
	public List<METAR> getMetar() { 
		
		String url = "https://www.aviationweather.gov/adds/dataserver_current/httpparam?dataSource=metars&requestType=retrieve&format=xml&stationString=KATL&hoursBeforeNow=1" ;
		
		RestTemplate restTemplate = new RestTemplate();
	     
		Response resp = restTemplate.getForObject( url, Response.class) ; 		
	    Data data = resp.getData() ; 
	    List<METAR> metars =  data.getMETAR() ; 
	    List<METAR> metarsRA = new ArrayList<> () ; 
	    
	    System.out.println( "num_result = " +  data.getNumResults()  + "\n" );
	    
		for( METAR metar : metars ) { 
		
			METAR newM = new METAR() ;
			
			newM.setObservationTime(metar.getObservationTime());
			newM.setVisibilityStatuteMi(metar.getVisibilityStatuteMi());
			newM.setWindSpeedKt(metar.getWindSpeedKt());
			
			
			System.out.println ("Wind guest: " + metar.getWindGustKt() ) ;
			System.out.println( "Visibility: " + metar.getVisibilityStatuteMi() );
			System.out.println( "Wind Speed: " + metar.getWindSpeedKt() ); 
			System.out.println( "Flight category: " + metar.getFlightCategory() );
			System.out.println( "Observation Time: "  + metar.getObservationTime());
			
			List<SkyCondition> skyCons = metar.getSkyCondition() ;
			System.out.println( "Sky Condition Count = " +  skyCons.size()  + "\n" );
			
			SkyCondition sc = skyCons.stream().min( (s1, s2) ->  Integer.compare( s1.getCloudBaseFtAgl(), s2.getCloudBaseFtAgl())).get() ;  
		    
			newM.addSkyCondition(sc);  
			
			for ( SkyCondition skyCon : skyCons ) { 
			
				System.out.println( "Sky Condition: " ) ; 
				System.out.println( "\tSky Cover: "+ skyCon.getSkyCover() ) ; 
				System.out.println( "\tCloud Base: "+skyCon.getCloudBaseFtAgl() ) ;
			}
			
			metarsRA.add(newM) ;
			
			System.out.println ( " ------------------- ") ; 			
		}
		
		// return restTemplate.getForObject( url, String.class)  ;	     
		
		return metarsRA ;
	}
	
	@RequestMapping ("/tafs")
	public void getTafs () { 
		
		String url =  "https://www.aviationweather.gov/adds/dataserver_current/httpparam?dataSource=tafs&requestType=retrieve&format=xml&hoursBeforeNow=24&timeType=issue&mostRecent=true&stationString=KATL" ;
		
		RestTemplate restTemp = new RestTemplate() ; 
		com.weather.xml.tafs.Response resp = restTemp.getForObject( url, com.weather.xml.tafs.Response.class) ; 
		
		com.weather.xml.tafs.TAF taf =  resp.getData().getTAF().get(0) ; 
		
		String stationID = taf.getStationId() ; 
		String issueTime = taf.getIssueTime() ;
		String validFrom = taf.getValidTimeFrom() ; 
		String ValidTo = taf.getValidTimeTo() ; 
		
		System.out.println ( "Station ID: " + stationID) ; 
		System.out.println ( "Issue Time: " + issueTime) ; 
		System.out.println ( "Valid From: " + validFrom) ; 
		System.out.println ( "Valid To: " + ValidTo) ; 
		
		System.out.println ( "" ) ; 
		
		List<com.weather.xml.tafs.Forecast> forecasts = taf.getForecast() ; 
		for (com.weather.xml.tafs.Forecast forecast : forecasts) { 
			
			System.out.println( "From: " + forecast.getFcstTimeFrom() );
			System.out.println( "To: " + forecast.getFcstTimeTo() );
			System.out.println( "Visibility Mile: " + forecast.getVisibilityStatuteMi() );
			
			System.out.println( "Condition: " + forecast.getSkyCondition().size() );
			for (com.weather.xml.tafs.SkyCondition skd : forecast.getSkyCondition() ) { 
			
				System.out.println( "\t" + skd.getSkyCover());
				System.out.println( "\t" + skd.getCloudBaseFtAgl() ) ; 
			}
			
			System.out.println ( " ------------------- ") ; 
		}
	}
	
	@RequestMapping ("/weather")
	public List<List<WeatherData>> getWeather () {
	
        return socketCont.getWeather() ;
	}
}
