package com.springboot.demo.airportObj.weather.datasource;

import org.springframework.context.ApplicationEvent;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;


@JsonIgnoreProperties(ignoreUnknown = true)
public class Notam_Data // extends ApplicationEvent {
{
	private final ReportType reportType = ReportType.NOTAMS;
	private String _id ; 
	private String _class ; 
	private String type ; 
	@JsonProperty
	private Notam_DataBody body ; 
	private String lat ;
	private String lon ; 
	private String ils_risk ; 
	private String rwy_risk ;
	private String notam_part ; 


	public Notam_Data () { } 
	
	/*
	public Notam_Data(Object source) {
		super(source);
		// TODO Auto-generated constructor stub
	}	
	*/
	
	public String getNotam_part() {
		return notam_part;
	}

	public void setNotam_part(String notam_part) {
		this.notam_part = notam_part;
	} 
	
	
	
	public String getLat() {
		return lat;
	}

	public void setLat(String lat) {
		this.lat = lat;
	}

	public String getLon() {
		return lon;
	}

	public void setLon(String lon) {
		this.lon = lon;
	}

	public String getIls_risk() {
		return ils_risk;
	}

	public void setIls_risk(String ils_risk) {
		this.ils_risk = ils_risk;
	}

	public String getRwy_risk() {
		return rwy_risk;
	}

	public void setRwy_risk(String rwy_risk) {
		this.rwy_risk = rwy_risk;
	}



	public String get_class() {
		return _class;
	}

	public void set_class(String _class) {
		this._class = _class;
	}
		
	public void setType(String type) {
		this.type = type;
	}

	public Notam_DataBody getBody() {
		return body;
	}

	public void setBody(Notam_DataBody body) {
		
		
		this.body = body;
	}

	public ReportType getType() {
		return reportType;
	}

	public String get_id() {
		return _id;
	}

	public void set_id(String _id) {
		this._id = _id;
	}

}

/*
{
"body": {
	"map": {
		"xmlns:gml": "http://www.opengis.net/gml",
		"notam_expire_time": "20190114T175900Z",
"notam_lastmod_time": "20180723T155700Z",
"notam_report": "8/8086 CANCELLED BY FDC 8/1839 ON 07/23/18 15:57",
"notam_qcode": "QPICH",
"insertion_time": "20180723T155711.420921Z",
"total_parts": 1,
"notam_cancel_time": "20180723T155711.420921Z",
"notam_effective_time": "20180604T175900Z",
"notam_id": "8/8086",
"notam_delete_time": "20180723T155711.420921Z",
"deletion_time": "20180723T155711.420921Z",
"storage_time": "20180723T155711.420921Z",
"account_id": "FDC",
"cns_location_id": "ATL",
"report_number": 1,
"icao_name": "HARTSFIELD - JACKSON ATLANTA INTL",
"gml:coordinates": "-84.428,33.637",
"icao_id": "KATL",
"source_id": "F",
"notam_text": "!FDC 8/8086 ATL CANCELLED BY FDC 8/1839 ON 07/23/18 15:57",
"notam_part": 1
}
},
"_id": "5b55fad95cdccd3b3dd82bfe",
"_class": "com.ra.rads.kafka.models.NotamObject",
"type": "notam",
"icao": "KATL",
"lat": "33.6558",
"lon": "-84.4333",
"ils_risk": false,
"rwy_risk": false
}
*/