package com.springboot.demo.airportObj.weather.datasource;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Notam_DataBody {

	@JsonProperty
	private Notam_DataMap map ;
	
	public Notam_DataMap getData() {
		return map;
	}

	public void setData( Notam_DataMap _map) {
		this.map = _map;
	}	
	
}
