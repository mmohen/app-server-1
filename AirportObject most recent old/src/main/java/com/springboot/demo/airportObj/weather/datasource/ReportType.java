package com.springboot.demo.airportObj.weather.datasource;

public enum ReportType {

   METAR, 
   TAFS, 
   NOTAMS 
}
	
