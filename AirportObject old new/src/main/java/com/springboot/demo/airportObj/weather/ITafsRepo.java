package com.springboot.demo.airportObj.weather;

import java.util.List;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.mongodb.repository.MongoRepository;
import com.springboot.demo.airportObj.weather.datasource.WeatherData;

public interface ITafsRepo extends MongoRepository<WeatherData, String> {

	
}