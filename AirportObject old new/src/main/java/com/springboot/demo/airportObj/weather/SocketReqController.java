package com.springboot.demo.airportObj.weather;

import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;

import com.springboot.demo.airportObj.weather.datasource.METAR_Data;
import com.springboot.demo.airportObj.weather.datasource.TAFS_Data;
import com.springboot.demo.airportObj.weather.datasource.WeatherData;

@Controller
public class SocketReqController {
	

	@Autowired
	WeatherBLL wbll ;
	
	String data = "None" ;
	List<List<WeatherData>> weatherList ;
	
	@PostConstruct
	public void setupListener () { 
		
		wbll.setWeatherListener( new IWeatherListener() {

			@Override
			public void newWeatherComing(METAR_Data metar) {
				
				
			}

			@Override
			public void newWeatherComing(TAFS_Data tafs) {
				
				// System.out.println("Controller TAFS: " + tafs.toString() );
				
			}
					

			@Override
			public void newWeatherComing(List<List<WeatherData>> weatherDataList) {
			
				  
				  try {
					
					  weatherList = weatherDataList ;
					  data = weatherDataList.toString() ;
					  updateWeather() ;
					  
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} 
				
			}
			
		} );
		
	}
	
	
	@SendTo("/topic/greetings")
	public String updateWeather () { 
		
		
		return data ;
	}
	
   
	public List<List<WeatherData>> getWeather() { 
		
		return weatherList ;
	}
}
