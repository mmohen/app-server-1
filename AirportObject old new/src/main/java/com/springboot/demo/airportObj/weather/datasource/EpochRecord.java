/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.springboot.demo.airportObj.weather.datasource;

public class EpochRecord {
    private long epochStartTime;
    private long epochEndTime;
    private String epoch_slot;

    private Integer ATLDepartureNASSACount_dep_0000;
    private Integer ATLDeparturePHIILCount_dep_0000;
    private Integer ATLDepartureSMKEYCount_dep_0000;
    private Integer ATLDepartureVRSTYCount_dep_0000;

    private String ATLArrivalCHPPRCount;
    private String ATLArrivalHOBTTCount;
    private String ATLArrivalJJEDICount;
    private String ATLArrivalONDRECount;
    private String ATLDepartureNASSACount;
    private String ATLDeparturePHIILCount;
    private String ATLDepartureSMKEYCount;
    private String ATLDepartureVRSTYCount;
    private String ATLArrivalCHPPRCount1;
    private String ATLArrivalHOBTTCount1;
    private String ATLArrivalJJEDICount1;
    private String ATLArrivalONDRECount1;
    private String ATLDepartureNASSACount1;
    private String ATLDeparturePHIILCount1;
    private String ATLDepartureSMKEYCount1;
    private String ATLDepartureVRSTYCount1;
    private String ATLArrivalCHPPRCount2;
    private String ATLArrivalHOBTTCount2;
    private String ATLArrivalJJEDICount2;
    private String ATLArrivalONDRECount2;
    private String ATLDepartureNASSACount2;
    private String ATLDeparturePHIILCount2;
    private String ATLDepartureSMKEYCount2;
    private String ATLDepartureVRSTYCount2;

    private String ATLArrivalCHPPRCount3;
    private String ATLArrivalHOBTTCount3;
    private String ATLArrivalJJEDICount3;
    private String ATLArrivalONDRECount3;
    private String ATLDepartureNASSACount3;
    private String ATLDeparturePHIILCount3;
    private String ATLDepartureSMKEYCount3;
    private String ATLDepartureVRSTYCount3;

    private String ATLArrivalCHPPRCount4;
    private String ATLArrivalHOBTTCount4;
    private String ATLArrivalJJEDICount4;
    private String ATLArrivalONDRECount4;
    private String ATLDepartureNASSACount4;
    private String ATLDeparturePHIILCount4;
    private String ATLDepartureSMKEYCount4;
    private String ATLDepartureVRSTYCount4;

    private String ATLArrivalCHPPRCount5;
    private String ATLArrivalHOBTTCount5;
    private String ATLArrivalJJEDICount5;
    private String ATLArrivalONDRECount5;
    private String ATLDepartureNASSACount5;
    private String ATLDeparturePHIILCount5;
    private String ATLDepartureSMKEYCount5;
    private String ATLDepartureVRSTYCount5;

    private String ATLArrivalCHPPRCount6;
    private String ATLArrivalHOBTTCount6;
    private String ATLArrivalJJEDICount6;
    private String ATLArrivalONDRECount6;
    private String ATLDepartureNASSACount6;
    private String ATLDeparturePHIILCount6;
    private String ATLDepartureSMKEYCount6;
    private String ATLDepartureVRSTYCount6;

    private String ATLArrivalCHPPRCount7;
    private String ATLArrivalHOBTTCount7;
    private String ATLArrivalJJEDICount7;
    private String ATLArrivalONDRECount7;
    private String ATLDepartureNASSACount7;
    private String ATLDeparturePHIILCount7;
    private String ATLDepartureSMKEYCount7;
    private String ATLDepartureVRSTYCount7;

    private String ATLArrivalCHPPRCount8;
    private String ATLArrivalHOBTTCount8;
    private String ATLArrivalJJEDICount8;
    private String ATLArrivalONDRECount8;
    private String ATLDepartureNASSACount8;
    private String ATLDeparturePHIILCount8;
    private String ATLDepartureSMKEYCount8;
    private String ATLDepartureVRSTYCount8;

    public String getEpoch_slot() {
        return epoch_slot;
    }

    public void setEpoch_slot(String epoch_slot) {
        this.epoch_slot = epoch_slot;
    }

    public EpochRecord() {
        super();
        epochStartTime = 0;
        epochEndTime = 0;
        epoch_slot = "";
        ATLArrivalCHPPRCount = null;
        ATLArrivalHOBTTCount = null;
        ATLArrivalJJEDICount = null;
        ATLArrivalONDRECount = null;

        ATLDepartureNASSACount_dep_0000 = 0;
        ATLDeparturePHIILCount_dep_0000 = 0;
        ATLDepartureSMKEYCount_dep_0000 = 0;
        ATLDepartureVRSTYCount_dep_0000 = 0;

        ATLDepartureNASSACount = null;
        ATLDeparturePHIILCount = null;
        ATLDepartureSMKEYCount = null;
        ATLDepartureVRSTYCount = null;
        ATLArrivalCHPPRCount1 = null;
        ATLArrivalHOBTTCount1 = null;
        ATLArrivalJJEDICount1 = null;
        ATLArrivalONDRECount1 = null;
        ATLDepartureNASSACount1 = null;
        ATLDeparturePHIILCount1 = null;
        ATLDepartureSMKEYCount1 = null;
        ATLDepartureVRSTYCount1 = null;
        ATLArrivalCHPPRCount2 = null;
        ATLArrivalHOBTTCount2 = null;
        ATLArrivalJJEDICount2 = null;
        ATLArrivalONDRECount2 = null;
        ATLDepartureNASSACount2 = null;
        ATLDeparturePHIILCount2 = null;
        ATLDepartureSMKEYCount2 = null;
        ATLDepartureVRSTYCount2 = null;

        ATLArrivalCHPPRCount3 = null;
        ATLArrivalHOBTTCount3 = null;
        ATLArrivalJJEDICount3 = null;
        ATLArrivalONDRECount3 = null;
        ATLDepartureNASSACount3 = null;
        ATLDeparturePHIILCount3 = null;
        ATLDepartureSMKEYCount3 = null;
        ATLDepartureVRSTYCount3 = null;

        ATLArrivalCHPPRCount4 = null;
        ATLArrivalHOBTTCount4 = null;
        ATLArrivalJJEDICount4 = null;
        ATLArrivalONDRECount4 = null;
        ATLDepartureNASSACount4 = null;
        ATLDeparturePHIILCount4 = null;
        ATLDepartureSMKEYCount4 = null;
        ATLDepartureVRSTYCount4 = null;

        ATLArrivalCHPPRCount5 = null;
        ATLArrivalHOBTTCount5 = null;
        ATLArrivalJJEDICount5 = null;
        ATLArrivalONDRECount5 = null;
        ATLDepartureNASSACount5 = null;
        ATLDeparturePHIILCount5 = null;
        ATLDepartureSMKEYCount5 = null;
        ATLDepartureVRSTYCount5 = null;

        ATLArrivalCHPPRCount6 = null;
        ATLArrivalHOBTTCount6 = null;
        ATLArrivalJJEDICount6 = null;
        ATLArrivalONDRECount6 = null;
        ATLDepartureNASSACount6 = null;
        ATLDeparturePHIILCount6 = null;
        ATLDepartureSMKEYCount6 = null;
        ATLDepartureVRSTYCount6 = null;

        ATLArrivalCHPPRCount7 = null;
        ATLArrivalHOBTTCount7 = null;
        ATLArrivalJJEDICount7 = null;
        ATLArrivalONDRECount7 = null;
        ATLDepartureNASSACount7 = null;
        ATLDeparturePHIILCount7 = null;
        ATLDepartureSMKEYCount7 = null;
        ATLDepartureVRSTYCount7 = null;

        ATLArrivalCHPPRCount8 = null;
        ATLArrivalHOBTTCount8 = null;
        ATLArrivalJJEDICount8 = null;
        ATLArrivalONDRECount8 = null;
        ATLDepartureNASSACount8 = null;
        ATLDeparturePHIILCount8 = null;
        ATLDepartureSMKEYCount8 = null;
        ATLDepartureVRSTYCount8 = null;
    }

    public EpochRecord(long newEpochStartTime, long newEpochEndTime, String epoch_slot) {
        super();
        epochStartTime = newEpochStartTime;
        epochEndTime = newEpochEndTime;
        epoch_slot = "";
        ATLArrivalCHPPRCount = null;
        ATLArrivalHOBTTCount = null;
        ATLArrivalJJEDICount = null;
        ATLArrivalONDRECount = null;
        ATLDepartureNASSACount = null;
        ATLDeparturePHIILCount = null;
        ATLDepartureSMKEYCount = null;
        ATLDepartureVRSTYCount = null;

        ATLDepartureNASSACount_dep_0000 = 0;
        ATLDeparturePHIILCount_dep_0000 = 0;
        ATLDepartureSMKEYCount_dep_0000 = 0;
        ATLDepartureVRSTYCount_dep_0000 = 0;

        ATLArrivalCHPPRCount1 = null;
        ATLArrivalHOBTTCount1 = null;
        ATLArrivalJJEDICount1 = null;
        ATLArrivalONDRECount1 = null;
        ATLDepartureNASSACount1 = null;
        ATLDeparturePHIILCount1 = null;
        ATLDepartureSMKEYCount1 = null;
        ATLDepartureVRSTYCount1 = null;
        ATLArrivalCHPPRCount2 = null;
        ATLArrivalHOBTTCount2 = null;
        ATLArrivalJJEDICount2 = null;
        ATLArrivalONDRECount2 = null;
        ATLDepartureNASSACount2 = null;
        ATLDeparturePHIILCount2 = null;
        ATLDepartureSMKEYCount2 = null;
        ATLDepartureVRSTYCount2 = null;

        ATLArrivalCHPPRCount3 = null;
        ATLArrivalHOBTTCount3 = null;
        ATLArrivalJJEDICount3 = null;
        ATLArrivalONDRECount3 = null;
        ATLDepartureNASSACount3 = null;
        ATLDeparturePHIILCount3 = null;
        ATLDepartureSMKEYCount3 = null;
        ATLDepartureVRSTYCount3 = null;

        ATLArrivalCHPPRCount4 = null;
        ATLArrivalHOBTTCount4 = null;
        ATLArrivalJJEDICount4 = null;
        ATLArrivalONDRECount4 = null;
        ATLDepartureNASSACount4 = null;
        ATLDeparturePHIILCount4 = null;
        ATLDepartureSMKEYCount4 = null;
        ATLDepartureVRSTYCount4 = null;

        ATLArrivalCHPPRCount5 = null;
        ATLArrivalHOBTTCount5 = null;
        ATLArrivalJJEDICount5 = null;
        ATLArrivalONDRECount5 = null;
        ATLDepartureNASSACount5 = null;
        ATLDeparturePHIILCount5 = null;
        ATLDepartureSMKEYCount5 = null;
        ATLDepartureVRSTYCount5 = null;

        ATLArrivalCHPPRCount6 = null;
        ATLArrivalHOBTTCount6 = null;
        ATLArrivalJJEDICount6 = null;
        ATLArrivalONDRECount6 = null;
        ATLDepartureNASSACount6 = null;
        ATLDeparturePHIILCount6 = null;
        ATLDepartureSMKEYCount6 = null;
        ATLDepartureVRSTYCount6 = null;

        ATLArrivalCHPPRCount7 = null;
        ATLArrivalHOBTTCount7 = null;
        ATLArrivalJJEDICount7 = null;
        ATLArrivalONDRECount7 = null;
        ATLDepartureNASSACount7 = null;
        ATLDeparturePHIILCount7 = null;
        ATLDepartureSMKEYCount7 = null;
        ATLDepartureVRSTYCount7 = null;

        ATLArrivalCHPPRCount8 = null;
        ATLArrivalHOBTTCount8 = null;
        ATLArrivalJJEDICount8 = null;
        ATLArrivalONDRECount8 = null;
        ATLDepartureNASSACount8 = null;
        ATLDeparturePHIILCount8 = null;
        ATLDepartureSMKEYCount8 = null;
        ATLDepartureVRSTYCount8 = null;
    }
    
    public long getEpochStartTime() {
        return epochStartTime;
    }
    
    public long getEpochEndTime() {
        return epochEndTime;
    }
    
    public void setEpochStartTime(long newEpochStartTime) {
        epochStartTime = newEpochStartTime;
    }
    
    public void setEpochEndTime(long newEpochEndTime) {
        epochEndTime = newEpochEndTime;
    }

    public String getATLArrivalCHPPRCount() {
        return ATLArrivalCHPPRCount;
    }

    public String getATLArrivalHOBTTCount() {
        return ATLArrivalHOBTTCount;
    }

    public String getATLArrivalJJEDICount() {
        return ATLArrivalJJEDICount;
    }

    public String getATLArrivalONDRECount() {
        return ATLArrivalONDRECount;
    }

    public String getATLDepartureNASSACount() {
        return ATLDepartureNASSACount;
    }

    public String getATLDeparturePHIILCount() {
        return ATLDeparturePHIILCount;
    }

    public String getATLDepartureSMKEYCount() {
        return ATLDepartureSMKEYCount;
    }

    public String getATLDepartureVRSTYCount() {
        return ATLDepartureVRSTYCount;
    }

    public void setATLArrivalCHPPRCount(String newATLArrivalCHPPRCount) {
        ATLArrivalCHPPRCount = newATLArrivalCHPPRCount;
    }

    public void setATLArrivalHOBTTCount(String newATLArrivalHOBTTCount) {
        ATLArrivalHOBTTCount = newATLArrivalHOBTTCount;
    }

    public String getATLArrivalCHPPRCount1() {
        return ATLArrivalCHPPRCount1;
    }

    public void setATLArrivalCHPPRCount1(String ATLArrivalCHPPRCount1) {
        this.ATLArrivalCHPPRCount1 = ATLArrivalCHPPRCount1;
    }

    public String getATLArrivalHOBTTCount1() {
        return ATLArrivalHOBTTCount1;
    }

    public void setATLArrivalHOBTTCount1(String ATLArrivalHOBTTCount1) {
        this.ATLArrivalHOBTTCount1 = ATLArrivalHOBTTCount1;
    }

    public String getATLArrivalJJEDICount1() {
        return ATLArrivalJJEDICount1;
    }

    public void setATLArrivalJJEDICount1(String ATLArrivalJJEDICount1) {
        this.ATLArrivalJJEDICount1 = ATLArrivalJJEDICount1;
    }

    public String getATLArrivalCHPPRCount2() {
        return ATLArrivalCHPPRCount2;
    }

    public void setATLArrivalCHPPRCount2(String ATLArrivalCHPPRCount2) {
        this.ATLArrivalCHPPRCount2 = ATLArrivalCHPPRCount2;
    }

    public String getATLArrivalHOBTTCount2() {
        return ATLArrivalHOBTTCount2;
    }

    public void setATLArrivalHOBTTCount2(String ATLArrivalHOBTTCount2) {
        this.ATLArrivalHOBTTCount2 = ATLArrivalHOBTTCount2;
    }

    public String getATLArrivalJJEDICount2() {
        return ATLArrivalJJEDICount2;
    }

    public void setATLArrivalJJEDICount2(String ATLArrivalJJEDICount2) {
        this.ATLArrivalJJEDICount2 = ATLArrivalJJEDICount2;
    }

    public String getATLArrivalONDRECount2() {
        return ATLArrivalONDRECount2;
    }

    public void setATLArrivalONDRECount2(String ATLArrivalONDRECount2) {
        this.ATLArrivalONDRECount2 = ATLArrivalONDRECount2;
    }

    public String getATLDepartureNASSACount2() {
        return ATLDepartureNASSACount2;
    }

    public void setATLDepartureNASSACount2(String ATLDepartureNASSACount2) {
        this.ATLDepartureNASSACount2 = ATLDepartureNASSACount2;
    }

    public String getATLDeparturePHIILCount2() {
        return ATLDeparturePHIILCount2;
    }

    public void setATLDeparturePHIILCount2(String ATLDeparturePHIILCount2) {
        this.ATLDeparturePHIILCount2 = ATLDeparturePHIILCount2;
    }

    public String getATLDepartureSMKEYCount2() {
        return ATLDepartureSMKEYCount2;
    }

    public void setATLDepartureSMKEYCount2(String ATLDepartureSMKEYCount2) {
        this.ATLDepartureSMKEYCount2 = ATLDepartureSMKEYCount2;
    }

    public String getATLDepartureVRSTYCount2() {
        return ATLDepartureVRSTYCount2;
    }

    public void setATLDepartureVRSTYCount2(String ATLDepartureVRSTYCount2) {
        this.ATLDepartureVRSTYCount2 = ATLDepartureVRSTYCount2;
    }

    public String getATLArrivalONDRECount1() {
        return ATLArrivalONDRECount1;
    }

    public void setATLArrivalONDRECount1(String ATLArrivalONDRECount1) {
        this.ATLArrivalONDRECount1 = ATLArrivalONDRECount1;
    }

    public String getATLArrivalCHPPRCount3() {
        return ATLArrivalCHPPRCount3;
    }

    public void setATLArrivalCHPPRCount3(String ATLArrivalCHPPRCount3) {
        this.ATLArrivalCHPPRCount3 = ATLArrivalCHPPRCount3;
    }

    public String getATLArrivalHOBTTCount3() {
        return ATLArrivalHOBTTCount3;
    }

    public void setATLArrivalHOBTTCount3(String ATLArrivalHOBTTCount3) {
        this.ATLArrivalHOBTTCount3 = ATLArrivalHOBTTCount3;
    }

    public String getATLArrivalJJEDICount3() {
        return ATLArrivalJJEDICount3;
    }

    public void setATLArrivalJJEDICount3(String ATLArrivalJJEDICount3) {
        this.ATLArrivalJJEDICount3 = ATLArrivalJJEDICount3;
    }

    public String getATLArrivalONDRECount3() {
        return ATLArrivalONDRECount3;
    }

    public void setATLArrivalONDRECount3(String ATLArrivalONDRECount3) {
        this.ATLArrivalONDRECount3 = ATLArrivalONDRECount3;
    }

    public String getATLDepartureNASSACount3() {
        return ATLDepartureNASSACount3;
    }

    public void setATLDepartureNASSACount3(String ATLDepartureNASSACount3) {
        this.ATLDepartureNASSACount3 = ATLDepartureNASSACount3;
    }

    public String getATLDeparturePHIILCount3() {
        return ATLDeparturePHIILCount3;
    }

    public void setATLDeparturePHIILCount3(String ATLDeparturePHIILCount3) {
        this.ATLDeparturePHIILCount3 = ATLDeparturePHIILCount3;
    }

    public String getATLDepartureSMKEYCount3() {
        return ATLDepartureSMKEYCount3;
    }

    public void setATLDepartureSMKEYCount3(String ATLDepartureSMKEYCount3) {
        this.ATLDepartureSMKEYCount3 = ATLDepartureSMKEYCount3;
    }

    public String getATLDepartureVRSTYCount3() {
        return ATLDepartureVRSTYCount3;
    }

    public void setATLDepartureVRSTYCount3(String ATLDepartureVRSTYCount3) {
        this.ATLDepartureVRSTYCount3 = ATLDepartureVRSTYCount3;
    }

    public String getATLArrivalCHPPRCount4() {
        return ATLArrivalCHPPRCount4;
    }

    public void setATLArrivalCHPPRCount4(String ATLArrivalCHPPRCount4) {
        this.ATLArrivalCHPPRCount4 = ATLArrivalCHPPRCount4;
    }

    public String getATLArrivalHOBTTCount4() {
        return ATLArrivalHOBTTCount4;
    }

    public void setATLArrivalHOBTTCount4(String ATLArrivalHOBTTCount4) {
        this.ATLArrivalHOBTTCount4 = ATLArrivalHOBTTCount4;
    }

    public String getATLArrivalJJEDICount4() {
        return ATLArrivalJJEDICount4;
    }

    public void setATLArrivalJJEDICount4(String ATLArrivalJJEDICount4) {
        this.ATLArrivalJJEDICount4 = ATLArrivalJJEDICount4;
    }

    public String getATLArrivalONDRECount4() {
        return ATLArrivalONDRECount4;
    }

    public void setATLArrivalONDRECount4(String ATLArrivalONDRECount4) {
        this.ATLArrivalONDRECount4 = ATLArrivalONDRECount4;
    }

    public String getATLDepartureNASSACount4() {
        return ATLDepartureNASSACount4;
    }

    public void setATLDepartureNASSACount4(String ATLDepartureNASSACount4) {
        this.ATLDepartureNASSACount4 = ATLDepartureNASSACount4;
    }

    public String getATLDeparturePHIILCount4() {
        return ATLDeparturePHIILCount4;
    }

    public void setATLDeparturePHIILCount4(String ATLDeparturePHIILCount4) {
        this.ATLDeparturePHIILCount4 = ATLDeparturePHIILCount4;
    }

    public String getATLDepartureSMKEYCount4() {
        return ATLDepartureSMKEYCount4;
    }

    public void setATLDepartureSMKEYCount4(String ATLDepartureSMKEYCount4) {
        this.ATLDepartureSMKEYCount4 = ATLDepartureSMKEYCount4;
    }

    public String getATLDepartureVRSTYCount4() {
        return ATLDepartureVRSTYCount4;
    }

    public void setATLDepartureVRSTYCount4(String ATLDepartureVRSTYCount4) {
        this.ATLDepartureVRSTYCount4 = ATLDepartureVRSTYCount4;
    }

    public String getATLArrivalCHPPRCount5() {
        return ATLArrivalCHPPRCount5;
    }

    public void setATLArrivalCHPPRCount5(String ATLArrivalCHPPRCount5) {
        this.ATLArrivalCHPPRCount5 = ATLArrivalCHPPRCount5;
    }

    public String getATLArrivalHOBTTCount5() {
        return ATLArrivalHOBTTCount5;
    }

    public void setATLArrivalHOBTTCount5(String ATLArrivalHOBTTCount5) {
        this.ATLArrivalHOBTTCount5 = ATLArrivalHOBTTCount5;
    }

    public String getATLArrivalJJEDICount5() {
        return ATLArrivalJJEDICount5;
    }

    public void setATLArrivalJJEDICount5(String ATLArrivalJJEDICount5) {
        this.ATLArrivalJJEDICount5 = ATLArrivalJJEDICount5;
    }

    public String getATLArrivalONDRECount5() {
        return ATLArrivalONDRECount5;
    }

    public void setATLArrivalONDRECount5(String ATLArrivalONDRECount5) {
        this.ATLArrivalONDRECount5 = ATLArrivalONDRECount5;
    }

    public String getATLDepartureNASSACount5() {
        return ATLDepartureNASSACount5;
    }

    public void setATLDepartureNASSACount5(String ATLDepartureNASSACount5) {
        this.ATLDepartureNASSACount5 = ATLDepartureNASSACount5;
    }

    public String getATLDeparturePHIILCount5() {
        return ATLDeparturePHIILCount5;
    }

    public void setATLDeparturePHIILCount5(String ATLDeparturePHIILCount5) {
        this.ATLDeparturePHIILCount5 = ATLDeparturePHIILCount5;
    }

    public String getATLDepartureSMKEYCount5() {
        return ATLDepartureSMKEYCount5;
    }

    public void setATLDepartureSMKEYCount5(String ATLDepartureSMKEYCount5) {
        this.ATLDepartureSMKEYCount5 = ATLDepartureSMKEYCount5;
    }

    public String getATLDepartureVRSTYCount5() {
        return ATLDepartureVRSTYCount5;
    }

    public void setATLDepartureVRSTYCount5(String ATLDepartureVRSTYCount5) {
        this.ATLDepartureVRSTYCount5 = ATLDepartureVRSTYCount5;
    }

    public String getATLArrivalCHPPRCount6() {
        return ATLArrivalCHPPRCount6;
    }

    public void setATLArrivalCHPPRCount6(String ATLArrivalCHPPRCount6) {
        this.ATLArrivalCHPPRCount6 = ATLArrivalCHPPRCount6;
    }

    public String getATLArrivalHOBTTCount6() {
        return ATLArrivalHOBTTCount6;
    }

    public void setATLArrivalHOBTTCount6(String ATLArrivalHOBTTCount6) {
        this.ATLArrivalHOBTTCount6 = ATLArrivalHOBTTCount6;
    }

    public String getATLArrivalJJEDICount6() {
        return ATLArrivalJJEDICount6;
    }

    public void setATLArrivalJJEDICount6(String ATLArrivalJJEDICount6) {
        this.ATLArrivalJJEDICount6 = ATLArrivalJJEDICount6;
    }

    public String getATLArrivalONDRECount6() {
        return ATLArrivalONDRECount6;
    }

    public void setATLArrivalONDRECount6(String ATLArrivalONDRECount6) {
        this.ATLArrivalONDRECount6 = ATLArrivalONDRECount6;
    }

    public String getATLDepartureNASSACount6() {
        return ATLDepartureNASSACount6;
    }

    public void setATLDepartureNASSACount6(String ATLDepartureNASSACount6) {
        this.ATLDepartureNASSACount6 = ATLDepartureNASSACount6;
    }

    public String getATLDeparturePHIILCount6() {
        return ATLDeparturePHIILCount6;
    }

    public void setATLDeparturePHIILCount6(String ATLDeparturePHIILCount6) {
        this.ATLDeparturePHIILCount6 = ATLDeparturePHIILCount6;
    }

    public String getATLDepartureSMKEYCount6() {
        return ATLDepartureSMKEYCount6;
    }

    public void setATLDepartureSMKEYCount6(String ATLDepartureSMKEYCount6) {
        this.ATLDepartureSMKEYCount6 = ATLDepartureSMKEYCount6;
    }

    public String getATLDepartureVRSTYCount6() {
        return ATLDepartureVRSTYCount6;
    }

    public void setATLDepartureVRSTYCount6(String ATLDepartureVRSTYCount6) {
        this.ATLDepartureVRSTYCount6 = ATLDepartureVRSTYCount6;
    }

    public String getATLArrivalCHPPRCount7() {
        return ATLArrivalCHPPRCount7;
    }

    public void setATLArrivalCHPPRCount7(String ATLArrivalCHPPRCount7) {
        this.ATLArrivalCHPPRCount7 = ATLArrivalCHPPRCount7;
    }

    public String getATLArrivalHOBTTCount7() {
        return ATLArrivalHOBTTCount7;
    }

    public void setATLArrivalHOBTTCount7(String ATLArrivalHOBTTCount7) {
        this.ATLArrivalHOBTTCount7 = ATLArrivalHOBTTCount7;
    }

    public String getATLArrivalJJEDICount7() {
        return ATLArrivalJJEDICount7;
    }

    public void setATLArrivalJJEDICount7(String ATLArrivalJJEDICount7) {
        this.ATLArrivalJJEDICount7 = ATLArrivalJJEDICount7;
    }

    public String getATLArrivalONDRECount7() {
        return ATLArrivalONDRECount7;
    }

    public void setATLArrivalONDRECount7(String ATLArrivalONDRECount7) {
        this.ATLArrivalONDRECount7 = ATLArrivalONDRECount7;
    }

    public String getATLDepartureNASSACount7() {
        return ATLDepartureNASSACount7;
    }

    public void setATLDepartureNASSACount7(String ATLDepartureNASSACount7) {
        this.ATLDepartureNASSACount7 = ATLDepartureNASSACount7;
    }

    public String getATLDeparturePHIILCount7() {
        return ATLDeparturePHIILCount7;
    }

    public void setATLDeparturePHIILCount7(String ATLDeparturePHIILCount7) {
        this.ATLDeparturePHIILCount7 = ATLDeparturePHIILCount7;
    }

    public String getATLDepartureSMKEYCount7() {
        return ATLDepartureSMKEYCount7;
    }

    public void setATLDepartureSMKEYCount7(String ATLDepartureSMKEYCount7) {
        this.ATLDepartureSMKEYCount7 = ATLDepartureSMKEYCount7;
    }

    public String getATLDepartureVRSTYCount7() {
        return ATLDepartureVRSTYCount7;
    }

    public void setATLDepartureVRSTYCount7(String ATLDepartureVRSTYCount7) {
        this.ATLDepartureVRSTYCount7 = ATLDepartureVRSTYCount7;
    }

    public String getATLArrivalCHPPRCount8() {
        return ATLArrivalCHPPRCount8;
    }

    public void setATLArrivalCHPPRCount8(String ATLArrivalCHPPRCount8) {
        this.ATLArrivalCHPPRCount8 = ATLArrivalCHPPRCount8;
    }

    public String getATLArrivalHOBTTCount8() {
        return ATLArrivalHOBTTCount8;
    }

    public void setATLArrivalHOBTTCount8(String ATLArrivalHOBTTCount8) {
        this.ATLArrivalHOBTTCount8 = ATLArrivalHOBTTCount8;
    }

    public String getATLArrivalJJEDICount8() {
        return ATLArrivalJJEDICount8;
    }

    public void setATLArrivalJJEDICount8(String ATLArrivalJJEDICount8) {
        this.ATLArrivalJJEDICount8 = ATLArrivalJJEDICount8;
    }

    public String getATLArrivalONDRECount8() {
        return ATLArrivalONDRECount8;
    }

    public void setATLArrivalONDRECount8(String ATLArrivalONDRECount8) {
        this.ATLArrivalONDRECount8 = ATLArrivalONDRECount8;
    }

    public String getATLDepartureNASSACount8() {
        return ATLDepartureNASSACount8;
    }

    public void setATLDepartureNASSACount8(String ATLDepartureNASSACount8) {
        this.ATLDepartureNASSACount8 = ATLDepartureNASSACount8;
    }

    public String getATLDeparturePHIILCount8() {
        return ATLDeparturePHIILCount8;
    }

    public void setATLDeparturePHIILCount8(String ATLDeparturePHIILCount8) {
        this.ATLDeparturePHIILCount8 = ATLDeparturePHIILCount8;
    }

    public String getATLDepartureSMKEYCount8() {
        return ATLDepartureSMKEYCount8;
    }

    public void setATLDepartureSMKEYCount8(String ATLDepartureSMKEYCount8) {
        this.ATLDepartureSMKEYCount8 = ATLDepartureSMKEYCount8;
    }

    public String getATLDepartureVRSTYCount8() {
        return ATLDepartureVRSTYCount8;
    }

    public void setATLDepartureVRSTYCount8(String ATLDepartureVRSTYCount8) {
        this.ATLDepartureVRSTYCount8 = ATLDepartureVRSTYCount8;
    }

    public String getATLDepartureNASSACount1() {
        return ATLDepartureNASSACount1;
    }

    public void setATLDepartureNASSACount1(String ATLDepartureNASSACount1) {
        this.ATLDepartureNASSACount1 = ATLDepartureNASSACount1;
    }

    public String getATLDeparturePHIILCount1() {
        return ATLDeparturePHIILCount1;
    }

    public void setATLDeparturePHIILCount1(String ATLDeparturePHIILCount1) {
        this.ATLDeparturePHIILCount1 = ATLDeparturePHIILCount1;
    }

    public String getATLDepartureSMKEYCount1() {
        return ATLDepartureSMKEYCount1;
    }

    public void setATLDepartureSMKEYCount1(String ATLDepartureSMKEYCount1) {
        this.ATLDepartureSMKEYCount1 = ATLDepartureSMKEYCount1;
    }

    public String getATLDepartureVRSTYCount1() {
        return ATLDepartureVRSTYCount1;
    }

    public void setATLDepartureVRSTYCount1(String ATLDepartureVRSTYCount1) {
        this.ATLDepartureVRSTYCount1 = ATLDepartureVRSTYCount1;
    }

    public void setATLArrivalJJEDICount(String newATLArrivalJJEDICount) {
        ATLArrivalJJEDICount = newATLArrivalJJEDICount;
    }

    public void setATLArrivalONDRECount(String newATLArrivalONDRECount) {
        ATLArrivalONDRECount = newATLArrivalONDRECount;
    }

    public void setATLDepartureNASSACount(String newATLDepartureNASSACount) {
        ATLDepartureNASSACount = newATLDepartureNASSACount;
    }

    public void setATLDeparturePHIILCount(String newATLDeparturePHIILCount) {
        ATLDeparturePHIILCount = newATLDeparturePHIILCount;
    }

    public void setATLDepartureSMKEYCount(String newATLDepartureSMKEYCount) {
        ATLDepartureSMKEYCount = newATLDepartureSMKEYCount;
    }

    public void setATLDepartureVRSTYCount(String newATLDepartureVRSTYCount) {
        ATLDepartureVRSTYCount = newATLDepartureVRSTYCount;
    }

    public Integer getATLDepartureNASSACount_dep_0000() {
        return ATLDepartureNASSACount_dep_0000;
    }

    public void setATLDepartureNASSACount_dep_0000(Integer ATLDepartureNASSACount_dep_0000) {
        this.ATLDepartureNASSACount_dep_0000 = ATLDepartureNASSACount_dep_0000;
    }

    public Integer getATLDeparturePHIILCount_dep_0000() {
        return ATLDeparturePHIILCount_dep_0000;
    }

    public void setATLDeparturePHIILCount_dep_0000(Integer ATLDeparturePHIILCount_dep_0000) {
        this.ATLDeparturePHIILCount_dep_0000 = ATLDeparturePHIILCount_dep_0000;
    }

    public Integer getATLDepartureSMKEYCount_dep_0000() {
        return ATLDepartureSMKEYCount_dep_0000;
    }

    public void setATLDepartureSMKEYCount_dep_0000(Integer ATLDepartureSMKEYCount_dep_0000) {
        this.ATLDepartureSMKEYCount_dep_0000 = ATLDepartureSMKEYCount_dep_0000;
    }

    public Integer getATLDepartureVRSTYCount_dep_0000() {
        return ATLDepartureVRSTYCount_dep_0000;
    }

    public void setATLDepartureVRSTYCount_dep_0000(Integer ATLDepartureVRSTYCount_dep_0000) {
        this.ATLDepartureVRSTYCount_dep_0000 = ATLDepartureVRSTYCount_dep_0000;
    }
}
