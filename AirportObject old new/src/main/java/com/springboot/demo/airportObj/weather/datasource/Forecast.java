package com.springboot.demo.airportObj.weather.datasource;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Forecast {

	private String fromTime ; 
	private String toTime ; 
	private float visibility ; 
	private Integer cloudBase ;
	private float windSpeetKt ; 
	
	private Date fromTimeDate ; 
	private Date toTimeDate ;
	SimpleDateFormat dateFormatter ;
	
	
	public Forecast() {
	
		super();
		
		dateFormatter = new SimpleDateFormat ( "yyyy-MM-dd'T'HH:mm:ss'Z'" ) ; 
	}

	public String getFromTime() {
		return fromTime;
	}

	public void setFromTime(String fromTime) {
		
		this.fromTime = fromTime;
		
		try {
		
			this.fromTimeDate = dateFormatter.parse(fromTime) ;
		
		} catch (ParseException e) {
		
			e.printStackTrace();
		} 		
	}

	public String getToTime() {
		return toTime;
	}

	public void setToTime(String toTime) {
	
		this.toTime = toTime;
		
		try {
			
			this.toTimeDate = dateFormatter.parse(toTime) ;
		} catch (ParseException e) {
			
			e.printStackTrace();
		} 
	
	}

	
	public Date getFromTimeDate() {
		return fromTimeDate;
	}

	public Date getToTimeDate() {
		return toTimeDate;
	}

	public float getVisibility() {
		return visibility;
	}

	public void setVisibility(float visibility) {
		this.visibility = visibility;
	}

	public Integer getCloudBase() {
		return cloudBase;
	}

	public void setCloudBase(Integer cloudBase) {
		this.cloudBase = cloudBase;
	}

	public float getWindSpeetKt() {
		return windSpeetKt;
	}

	public void setWindSpeetKt(float windSpeetKt) {
		this.windSpeetKt = windSpeetKt;
	}

	@Override
	public String toString() {
		return "Forecast [fromTime=" + fromTime + ", toTime=" + toTime + ", visibility=" + visibility + ", cloudBase="
				+ cloudBase + ", windSpeetKt=" + windSpeetKt + "]";
	}


}

