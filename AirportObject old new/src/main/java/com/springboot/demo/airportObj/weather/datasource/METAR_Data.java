package com.springboot.demo.airportObj.weather.datasource;

import org.springframework.context.ApplicationEvent;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.mongodb.core.mapping.Document;


@Document(collection = "metar")
public class METAR_Data extends ApplicationEvent{
	
	@Id
	public String id ;
	private final ReportType type = ReportType.METAR; 
	private String airportCode ;
	private String observationTime ; 
	private float visibilityStatusMi ; 
	private float windSpeetKt ;
	private Integer cloudBase ; 
	private Integer windDir ;
	
	
	public METAR_Data(Object source) {
		 super(source);
	
	}
		
	public String getAirportCode() {
		return airportCode;
	}
	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}
	public String getObservationTime() {
		return observationTime;
	}
	public void setObservationTime(String observationTime) {
		this.observationTime = observationTime;
	}
	public float getVisibility() {
		return visibilityStatusMi;
	}
	public void setVisibility(float visibilityStatusMi) {
		this.visibilityStatusMi = visibilityStatusMi;
	}
	public float getWindSpeetKt() {
		return windSpeetKt;
	}
	public void setWindSpeetKt(float windSpeetKt) {
		this.windSpeetKt = windSpeetKt;
	}
	
	public Integer getCloudBase() {
		return cloudBase;
	}
	public void setCloudBase(Integer cloudBase) {
		this.cloudBase = cloudBase;
	}
	
	public MetarDoc createMetarDoc () { 

		return new MetarDoc ( airportCode, observationTime, visibilityStatusMi,windSpeetKt,cloudBase, windDir) ;
	}
	
	@Override
	public String toString() {
		return "WeatherData [type=" + type + ", airportCode=" + airportCode + ", observationTime=" + observationTime
				+ ", visibilityStatusMi=" + visibilityStatusMi + ", windSpeetKt=" + windSpeetKt + ", cloudBase="
				+ cloudBase + "]";
	}

	public Integer getWindDir() {
		return windDir;
	}

	public void setWindDir(Integer windDir) {
		this.windDir = windDir;
	}

	
	

	
}
