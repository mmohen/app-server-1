package com.springboot.demo.airportObj.weather.datasource;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "metar")
public class MetarDoc {

	@Id
    public String id;

	private final ReportType type = ReportType.METAR; 
	private String airportCode ;
	private String observationTime ; 
	private float visibilityStatusMi ; 
	private float windSpeetKt ;
	private Integer cloudBase ; 
	private Integer windDirection ;
	
    public MetarDoc() {}

    public MetarDoc(String airportCode, String observationTime, float visibilityStatusMi, 
    		        float windSpeetKt, Integer cloudBase, Integer windDir ) {
       
    	this.airportCode = airportCode ; 
    	this.observationTime = observationTime ; 
    	this.visibilityStatusMi = visibilityStatusMi ; 
    	this.windSpeetKt = windSpeetKt ; 
    	this.cloudBase = cloudBase ; 
    	this.windDirection = windDir ;
    	
    }  
}
