package com.springboot.demo.airportObj.weather.datasource;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.ApplicationEvent;


public class TAFS_Data extends ApplicationEvent {
		

	/**
	 * 
	 */

	private final ReportType type = ReportType.TAFS ; 
	private String airportCode ;		
	private String issueTime ;
	private String validFrom ; 
	private String validTo ;
	private String epoch;
 	private List<Forecast> forecasts  = null ;

	public String getEpoch() {
		return epoch;
	}

	public void setEpoch(String epoch) {
		this.epoch = epoch;
	}

	public TAFS_Data(Object source) {
		super(source);
	}


	public ReportType getType() {
		return type;
	}


	public String getAirportCode() {
		return airportCode;
	}


	public String getIssueTime() {
		return issueTime;
	}

	public void setIssueTime(String issueTime) {
		this.issueTime = issueTime;
	}

	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}


	public String getValidFrom() {
		return validFrom;
	}


	public void setValidFrom(String validFrom) {
		this.validFrom = validFrom;
	}


	public String getValidTo() {
		return validTo;
	}


	public void setValidTo(String validTo) {
		this.validTo = validTo;
	}


	public List<Forecast> getForecasts() {
		return forecasts;
	}


	public void addForecast( Forecast  newForecast) {
		
		if ( forecasts == null ) { 
			
			forecasts = new ArrayList<Forecast>() ;
		}
		
		forecasts.add(newForecast); 
	}

	@Override
	public String toString() {
		return "TAFS_Data [type=" + type + ", airportCode=" + airportCode + ", issueTime=" + issueTime + ", validFrom="
				+ validFrom + ", validTo=" + validTo + ", forecasts=" + forecasts + "]";
	}
	
	
}
