package com.springboot.demo.airportObj.weather;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.springboot.demo.airportObj.weather.datasource.MetarDoc;

public interface IMetarRepo extends MongoRepository<MetarDoc, String> {

	
}
