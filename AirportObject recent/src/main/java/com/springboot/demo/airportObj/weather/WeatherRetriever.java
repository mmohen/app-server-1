package com.springboot.demo.airportObj.weather;


import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;
import java.util.stream.Collectors;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.PayloadApplicationEvent;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.springboot.demo.airportObj.weather.datasource.Forecast;
import com.springboot.demo.airportObj.weather.datasource.METAR_Data;
import com.springboot.demo.airportObj.weather.datasource.Notam_Data;
import com.springboot.demo.airportObj.weather.datasource.TAFS_Data;
import com.weather.xml.metar.Data;
import com.weather.xml.metar.METAR;
import com.weather.xml.metar.Response;
import com.weather.xml.metar.SkyCondition;


@Configuration
@EnableScheduling
@Service
public class WeatherRetriever {

	@Autowired
	private ApplicationEventPublisher applicationEventPublisher;

	private String lastNotamId = null ;
	private SimpleDateFormat dateFormatGmt = new SimpleDateFormat( "yyyyMMdd'T'HHmmss'Z'" ); 
	private Calendar calendar ; 

	public WeatherRetriever () { 

		dateFormatGmt.setTimeZone(TimeZone.getTimeZone("UTC"));	
		calendar = Calendar.getInstance( TimeZone.getTimeZone("UTC") );
	}


	@Scheduled(fixedRate = 3000)
	public void getMetar() { 

		String url = "https://www.aviationweather.gov/adds/dataserver_current/httpparam?dataSource=metars&requestType=retrieve&format=xml&stationString=KATL&hoursBeforeNow=1" ;

		RestTemplate restTemplate = new RestTemplate();

		Response resp = restTemplate.getForObject( url, Response.class) ; 		
		Data data = resp.getData() ; 

		if ( data.getNumResults() == 0 ) { 

			return ;
		} 

		List<METAR> metars =  data.getMETAR() ; 
		METAR_Data newM = new METAR_Data(this) ; 

		newM.setAirportCode("KATL");
		newM.setObservationTime(metars.get(0).getObservationTime());
		newM.setVisibility(metars.get(0).getVisibilityStatuteMi());
		newM.setWindSpeetKt(metars.get(0).getWindSpeedKt());
		newM.setWindDir(metars.get(0).getWindDirDegrees());
		
		List<SkyCondition> skyCons = metars.get(0).getSkyCondition() ;

		skyCons = skyCons.stream().filter(item -> item.getSkyCover().equals("BKN")||item.getSkyCover().equals("OVC")).collect(Collectors.toList()); 

		if ( skyCons.isEmpty() ) { 

			newM.setCloudBase(-1);
		}
		else { 

			SkyCondition sc = skyCons.stream().min( (s1, s2) -> Integer.compare( s1.getCloudBaseFtAgl(), s2.getCloudBaseFtAgl())).get() ;  
			newM.setCloudBase(sc.getCloudBaseFtAgl());  
		}
		applicationEventPublisher.publishEvent( newM ) ;
	}


	@Scheduled(fixedRate = 5000)
	public void getTafs () { 

		String url =  "https://www.aviationweather.gov/adds/dataserver_current/httpparam?dataSource=tafs&requestType=retrieve&format=xml&hoursBeforeNow=24&timeType=issue&mostRecent=true&stationString=KATL" ;

		RestTemplate restTemp = new RestTemplate() ; 
		com.weather.xml.tafs.Response resp = restTemp.getForObject( url, com.weather.xml.tafs.Response.class) ; 
		com.weather.xml.tafs.TAF taf =  resp.getData().getTAF().get(0) ; 

		TAFS_Data newTafs = new TAFS_Data(this) ; 

		newTafs.setAirportCode(taf.getStationId());
		newTafs.setIssueTime(taf.getIssueTime());
		newTafs.setValidFrom(taf.getValidTimeFrom());
		newTafs.setValidTo(taf.getValidTimeTo());
		

		List<com.weather.xml.tafs.Forecast> forecasts = taf.getForecast() ; 

		for (com.weather.xml.tafs.Forecast forecast : forecasts) { 

			Forecast newForecast = new Forecast () ; 

			newForecast.setFromTime(forecast.getFcstTimeFrom());
			newForecast.setToTime(forecast.getFcstTimeTo());
			newForecast.setVisibility(forecast.getVisibilityStatuteMi() == null ? -1 : forecast.getVisibilityStatuteMi()  );
			newForecast.setWindSpeetKt(forecast.getWindSpeedKt() == null ? -1 : forecast.getWindSpeedKt() );
			newForecast.setWindDirection(forecast.getWindDirDegrees() == null ? -1 : forecast.getWindDirDegrees());
			 
			
			List<com.weather.xml.tafs.SkyCondition> skyConds = forecast.getSkyCondition();

			skyConds = skyConds.stream().filter(item -> item.getSkyCover().equals("BKN")||item.getSkyCover().equals("OVC") ).collect(Collectors.toList()) ;

			if ( skyConds.isEmpty() ) { 

				newForecast.setCloudBase( -1 );

			}else { 

				com.weather.xml.tafs.SkyCondition skyCond = skyConds.stream().min( (s1, s2) ->  Integer.compare( s1.getCloudBaseFtAgl(), s2.getCloudBaseFtAgl())).get() ;
				newForecast.setCloudBase( skyCond.getCloudBaseFtAgl() );							
			}

			newTafs.addForecast(newForecast);
		}	

		applicationEventPublisher.publishEvent( newTafs ) ;
	}


	@Scheduled(fixedRate = 5000)
	public void getNotam () { 

		// String url = "http://ec2-18-188-42-129.us-east-2.compute.amazonaws.com:3000/api/notams/5/KATL" ;
		String url = "http://ec2-18-188-42-129.us-east-2.compute.amazonaws.com:3000/api/notams/" ; 

		try {

			Date curr = calendar.getTime() ; 
			List<Notam_Data> notamList = new ArrayList<Notam_Data>() ;

			boolean found = false ;
			boolean hasUpdate = false ;
			int recordNum = 0 ; 

			while ( !found ) { 

				recordNum += 5 ; 		    		
				url += recordNum + "/KATL" ;

				RestTemplate restTemp = new RestTemplate() ; 			

				Notam_Data[] forNow = restTemp.getForObject( url, Notam_Data[].class);		     
				notamList = Arrays.asList(forNow);	
			
				
				if ( !notamList.isEmpty() ) {  

					Notam_Data firstNode = notamList.get(0) ; 
                    Date firstDate = dateFormatGmt.parse( firstNode.getBody().getData().getNotam_expire_time() );
                    		
					if ( ( lastNotamId != null && lastNotamId.equals( firstNode.get_id() ) ) ||  curr.after(firstDate) ) { 
						
						break; 							
					}

					lastNotamId = firstNode.get_id() ;						

					Notam_Data lastNode = notamList.get( notamList.size() - 1 ) ; 
					Date lastDate = dateFormatGmt.parse( lastNode.getBody().getData().getNotam_expire_time() );


					if ( curr.after( lastDate ) ) {  

						notamList = notamList.stream().filter( item -> {

							try {

								return (dateFormatGmt.parse( item.getBody().getData().getNotam_expire_time())).after(curr);
							}
							catch (ParseException e) {

								e.printStackTrace();

								return false ;
							}

						} ).collect(Collectors.toList()) ;

						found = true ;
						hasUpdate = true ;
					}
					
					
				}    		
			}	

			if ( hasUpdate ) { 

				PayloadApplicationEvent<List<Notam_Data>> payload = new  PayloadApplicationEvent<List<Notam_Data>>( Notam_Data.class, notamList) ; 
				applicationEventPublisher.publishEvent( payload ) ;		   
			}

			/*
			RestTemplate restTemp = new RestTemplate() ; 			

			Notam_Data[] forNow = restTemp.getForObject( url, Notam_Data[].class);		     
			List<Notam_Data> searchList= Arrays.asList(forNow);			

			PayloadApplicationEvent<List<Notam_Data>> payload = new  PayloadApplicationEvent<List<Notam_Data>>( Notam_Data.class, searchList) ; 


			applicationEventPublisher.publishEvent( payload ) ;			
			 */

		} catch (Exception e){

			System.out.println("Unable to save Notam_Data: " + e.getMessage());
		}
	}

}









