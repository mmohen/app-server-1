package com.springboot.demo.airportObj.weather.datasource;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Notam_DataMap {

	private String  xmlns_gml ;    // might not bind
	private String  notam_expire_time ; 
	private String  notam_lastmod_time ; 
	private String  notam_report ; 
	private String  notam_qcode ; 
	private String  insertion_time ; 
	private String  total_parts ; 
	private String  notam_cancel_time ; 
	private String  notam_effective_time ; 
	private String  notam_id ; 
	private String  notam_delete_time ; 
	private String  deletion_time ; 
	private String  storage_time ; 
	private String  account_id ; 
	private String  cns_location_id ; 
	private String  report_number ; 
	private String  icao_name ; 
	private String  gml_coordinates ;  // might not bind
	private String  icao_id ; 
	private String  source_id ; 
	private String  notam_text ; 
	private String  notam_part ;
	
	public String getXmlns_gml() {
		return xmlns_gml;
	}
	public void setXmlns_gml(String xmlns_gml) {
		this.xmlns_gml = xmlns_gml;
	}
	public String getNotam_expire_time() {
		return notam_expire_time;
	}
	public void setNotam_expire_time(String notam_expire_time) {
		this.notam_expire_time = notam_expire_time;
	}
	public String getNotam_lastmod_time() {
		return notam_lastmod_time;
	}
	public void setNotam_lastmod_time(String notam_lastmod_time) {
		this.notam_lastmod_time = notam_lastmod_time;
	}
	public String getNotam_report() {
		return notam_report;
	}
	public void setNotam_report(String notam_report) {
		this.notam_report = notam_report;
	}
	public String getNotam_qcode() {
		return notam_qcode;
	}
	public void setNotam_qcode(String notam_qcode) {
		this.notam_qcode = notam_qcode;
	}
	public String getInsertion_time() {
		return insertion_time;
	}
	public void setInsertion_time(String insertion_time) {
		this.insertion_time = insertion_time;
	}
	public String getTotal_parts() {
		return total_parts;
	}
	public void setTotal_parts(String total_parts) {
		this.total_parts = total_parts;
	}
	public String getNotam_cancel_time() {
		return notam_cancel_time;
	}
	public void setNotam_cancel_time(String notam_cancel_time) {
		this.notam_cancel_time = notam_cancel_time;
	}
	public String getNotam_effective_time() {
		return notam_effective_time;
	}
	public void setNotam_effective_time(String notam_effective_time) {
		this.notam_effective_time = notam_effective_time;
	}
	public String getNotam_id() {
		return notam_id;
	}
	public void setNotam_id(String notam_id) {
		this.notam_id = notam_id;
	}
	public String getNotam_delete_time() {
		return notam_delete_time;
	}
	public void setNotam_delete_time(String notam_delete_time) {
		this.notam_delete_time = notam_delete_time;
	}
	public String getDeletion_time() {
		return deletion_time;
	}
	public void setDeletion_time(String deletion_time) {
		this.deletion_time = deletion_time;
	}
	public String getStorage_time() {
		return storage_time;
	}
	public void setStorage_time(String storage_time) {
		this.storage_time = storage_time;
	}
	public String getAccount_id() {
		return account_id;
	}
	public void setAccount_id(String account_id) {
		this.account_id = account_id;
	}
	public String getCns_location_id() {
		return cns_location_id;
	}
	public void setCns_location_id(String cns_location_id) {
		this.cns_location_id = cns_location_id;
	}
	public String getReport_number() {
		return report_number;
	}
	public void setReport_number(String report_number) {
		this.report_number = report_number;
	}
	public String getIcao_name() {
		return icao_name;
	}
	public void setIcao_name(String icao_name) {
		this.icao_name = icao_name;
	}
	public String getGml_coordinates() {
		return gml_coordinates;
	}
	public void setGml_coordinates(String gml_coordinates) {
		this.gml_coordinates = gml_coordinates;
	}
	public String getIcao_id() {
		return icao_id;
	}
	public void setIcao_id(String icao_id) {
		this.icao_id = icao_id;
	}
	public String getSource_id() {
		return source_id;
	}
	public void setSource_id(String source_id) {
		this.source_id = source_id;
	}
	public String getNotam_text() {
		return notam_text;
	}
	public void setNotam_text(String notam_text) {
		this.notam_text = notam_text;
	}
	public String getNotam_part() {
		return notam_part;
	}
	public void setNotam_part(String notam_part) {
		this.notam_part = notam_part;
	}	
}
