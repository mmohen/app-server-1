package com.springboot.demo.airportObj.weather.datasource;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection="tafs")
public class WeatherData {

	private ReportType type ; 
	private String airportCode ;
	private String time ; 
	private float visibilityStatusMi ; 
	private float windSpeetKt ;
	private Integer cloudBase ;
	private float windDirection ;
	
	
	public WeatherData() {
	
		super();
		
		visibilityStatusMi = -1 ;
		windSpeetKt = -1 ;
		cloudBase = -1 ;	
	}
		
	
	public ReportType getType() {
		return type;
	}
	public void setType(ReportType type) {
		this.type = type;
	}
	public String getAirportCode() {
		return airportCode;
	}
	public void setAirportCode(String airportCode) {
		this.airportCode = airportCode;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public float getVisibilityStatusMi() {
		return visibilityStatusMi;
	}
	public void setVisibilityStatusMi(float visibilityStatusMi) {
		this.visibilityStatusMi = visibilityStatusMi;
	}
	public float getWindSpeetKt() {
		return windSpeetKt;
	}
	public void setWindSpeetKt(float windSpeetKt) {
		this.windSpeetKt = windSpeetKt;
	}
	public Integer getCloudBase() {
		return cloudBase;
	}
	public void setCloudBase(Integer cloudBase) {
		this.cloudBase = cloudBase;
	}
	
	public float getWindDirection() {
		return windDirection;
	}


	public void setWindDirection(float windDirection) {
		this.windDirection = windDirection;
	} 
	
	@Override
	public String toString() {
		return "WeatherData [type=" + type + ", airportCode=" + airportCode + ", time=" + time + ", visibilityStatusMi="
				+ visibilityStatusMi + ", windSpeetKt=" + windSpeetKt + ", cloudBase=" + cloudBase + "]";
	}
	
}
