package com.ra.rads.activemq;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.listener.DefaultMessageListenerContainer;

import java.io.*;
import java.util.Properties;

@Configuration
public class JmsConfig {

    @Bean
    public ActiveMQConnectionFactory connectionFactory(){

        Properties prop = new Properties();
        InputStream input = null;
        String url = "";
        String username = "";
        String password = "";
        try {
            input = new FileInputStream("config.properties");
            prop.load(input);
            url = prop.getProperty("url");
            password = prop.getProperty("password");
            username = prop.getProperty("username");
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory();
        connectionFactory.setBrokerURL(url);
        connectionFactory.setPassword(password);
        connectionFactory.setUserName(username);
        //connectionFactory.setUseCompression(true);
        return connectionFactory;
    }

    @Bean
    public JmsTemplate jmsTemplate(){
        JmsTemplate template = new JmsTemplate();
        template.setConnectionFactory(connectionFactory());
        return template;
    }

    @Bean
    public DefaultJmsListenerContainerFactory jmsListenerContainerFactory() {
        DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();
        factory.setConnectionFactory(connectionFactory());
        factory.setConcurrency("10");
        return factory;
    }

}
