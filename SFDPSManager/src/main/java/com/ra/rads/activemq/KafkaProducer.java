package com.ra.rads.activemq;

import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Properties;
import java.util.UUID;

public class KafkaProducer {

    public void importK(JSONObject input, String guid) throws IOException {

        String topic_name = null;

        System.out.println(input);

        String contents = input.toString();

        Properties properties = new Properties();
        properties.setProperty("bootstrap.servers", "ec2-18-219-180-4.us-east-2.compute.amazonaws.com:9092");
        properties.setProperty("key.serializer", StringSerializer.class.getName());
        properties.setProperty("value.serializer", StringSerializer.class.getName());
        // producer acks
        properties.setProperty("acks", "1");
        properties.setProperty("retries", "3");
        properties.setProperty("linger.ms", "1");
        Producer<String, String> producer = new org.apache.kafka.clients.producer.KafkaProducer<String, String>(properties);
        String suuid = UUID.randomUUID().toString();
        ProducerRecord<String, String> producerRecord =
                new ProducerRecord<String, String>("SFDPS_INCOMING", contents);
        producer.send(producerRecord);
        producer.close();
    }

}
