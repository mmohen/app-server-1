package com.ra.rads.activemq;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class Listener {

    private static final long MEGABYTE = 1024L * 1024L;

    public static long bytesToMeg(long bytes) {
        return bytes / MEGABYTE;
    }

    private static final Logger LOG = LoggerFactory.getLogger(Listener.class);

    //Convert convert = new Convert();

    @JmsListener(destination = "SFDPS_FLIGHT_PLANS")
    public void receiveMessage(final Message message) throws JMSException {

        long startTime = System.nanoTime();

        LOG.info("Receiving message ...");

        String content = ((TextMessage) message).getText();

        //System.out.println(content);
        Map<String, Object> jsonMap = null;
        try {
            JSONObject jo = XML.toJSONObject(content);
            //System.out.println(jo);
            //jsonMap = JsonFlattener.flattenAsMap(jo.toString());
            //System.out.println(jsonMap);

            // put in kafka
            KafkaProducer kafka = new KafkaProducer();
            kafka.importK(jo, "");


        } catch (JSONException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
