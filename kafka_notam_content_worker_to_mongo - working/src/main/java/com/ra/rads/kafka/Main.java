package com.ra.rads.kafka;

import com.ra.rads.kafka.models.FlightObject;
import com.ra.rads.kafka.models.FlightObjectRecord;
import com.ra.rads.kafka.repositories.FlightObjectRepository;
import com.ra.rads.kafka.repositories.NotamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import java.util.Arrays;
import java.util.List;

@SpringBootApplication
@EnableMongoRepositories(basePackageClasses = FlightObjectRepository.class)
public class Main {

    @Autowired
    private FlightObjectRepository flightObjectRepository;

    @Autowired
    private NotamRepository notamRepository;

    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);
    }

//    @Override
//    public void run(String... strings) throws Exception {
//
//        final FlightObjectRecord forrcord = new FlightObjectRecord("fo",
//                "active", "UAL", "IAD", "123", "456",
//                "E190", "4:30am", "6:00pm", "100",
//                "BWI", "1234567890");
//        final FlightObjectRecord forrcord1 = new FlightObjectRecord("fo",
//                "active", "UAL", "IAD", "123", "456",
//                "E190", "4:30am", "6:00pm", "100",
//                "BWI", "1234567890");
//        final List<FlightObjectRecord> for1 = Arrays.asList(forrcord, forrcord1);
//        FlightObject fo = new FlightObject("1234567890", for1);
//        flightObjectRepository.save(fo);
//
//
//    }

}