package com.ra.rads.kafka.consumer;

import com.mongodb.BasicDBObject;
import com.ra.rads.kafka.flattener.JsonFlattener;
import com.ra.rads.kafka.models.Airport;
import com.ra.rads.kafka.models.NotamObject;
import com.ra.rads.kafka.repositories.AirportRepository;
import com.ra.rads.kafka.repositories.NotamRepository;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;
import org.json.simple.parser.ParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;
import org.apache.commons.io.IOUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

@Service
public class Receiver {

    private static final Logger LOG = LoggerFactory.getLogger(Receiver.class);

    @Autowired
    private NotamRepository notamRepository;

    @Autowired
    private AirportRepository airportRepository;

    int counter = 0;

    @KafkaListener(topics = "${app.topic.fo}")
    public void listen(ConsumerRecord<Integer, String> consumerRecord, @Payload String message) throws ParseException, JSONException {

        //LOG.info("received KAFKA Flight Object message='{}'", message);

        Map<String, Object> jsonMap = null;
        //System.out.printf("Processing message " + counter++ + "\n");
        //System.out.printf(message);

        try {
            org.json.JSONObject jo = XML.toJSONObject(message);
            String new_json = jo.toString();
            jsonMap = JsonFlattener.flattenAsMap(new_json);
            org.json.JSONObject new_jo = new org.json.JSONObject(jsonMap);

            String icao = (String) new_jo.get("icao_id");

            //String body = (String) new_jo.get("notam_text");

            String qcode = (String) new_jo.get("notam_qcode");

            boolean ils_risk = false;
            boolean rwy_risk = false;

            // ILS - UNSERVICEABLE
            if(qcode.equals("QICAS")) {
                System.out.println("ILS RISK! **********************");
              ils_risk = true;
            }

            // RNWAY - CLOSED
            if(qcode.equals("QMRLC")) {
                System.out.println("RWY RISK! **********************");
                rwy_risk = true;
            }

            // RNWAY - UNSERVICEABLE
            if(qcode.equals("QMRAS")) {
                System.out.println("RWY RISK! **********************");
                rwy_risk = true;
            }


//            if (body.matches("(?i).*OUT OF SERVICE.*") && body.matches("(?i).*RWY.*")) {
//                rwy_risk = true;
//                System.out.println("RWY RISK! **********************");
//            }
//            if (body.matches("(?i).*OUT OF SERVICE.*") && body.matches("(?i).*ILS.*")) {
//                ils_risk = true;
//                System.out.println("ILS RISK! **********************");
//            }


            // get lat long for this airport
            Airport airport = airportRepository.findByIcao(icao);
            if (airport == null) {
                System.out.println("Cannot find an ICAO code");
                NotamObject no = new NotamObject("notam", "UNK", "UNK", "UNK", ils_risk, rwy_risk, new_jo);
                notamRepository.save(no);
                //sendToLogStash(new_jo);
            } else {
                //System.out.println("Found an matching ICAO code");
                String airport_code = airport.getIcao();
                String lat = airport.getLat();
                String lon = airport.getLon();

                NotamObject no = new NotamObject("notam", airport_code, lat, lon, ils_risk, rwy_risk, new_jo);
                notamRepository.save(no);

                // append json object
                new_jo.put("lat", lat);
                new_jo.put("lon", lon);

                //sendToLogStash(new_jo);

            }

            System.out.printf("Processing message " + counter++ + "\n");



        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    public void sendToLogStash(JSONObject new_jo) {
        try {
                URL url = new URL(
                        "http://18.221.152.14:8003");
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                JSONObject obj = new JSONObject();
                conn.setDoOutput(true);
                conn.setRequestMethod("PUT");
                conn.setRequestProperty("Content-Type", "application/json");
                obj.put("app", "NOTAM");
                obj.put("messageType", new_jo);
                String input = obj.toString();
                OutputStream os = conn.getOutputStream();
                os.write(input.getBytes());
                os.flush();
                BufferedReader br = new BufferedReader(new InputStreamReader(
                        (conn.getInputStream())));
                String output;
                while ((output = br.readLine()) != null) {
                    System.out.println(output);
                }
                conn.disconnect();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}
