package com.ra.rads.kafka.models;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@ToString(exclude = {"id"})
@Document(collection = "airports")
public class Airport {

    @Id private String id;
    private String icao;
    private String lat;
    private String lon;

    public Airport(
            final String icao,
            final String lat,
            final String lon) {
        this.icao = icao;
        this.lat = lat;
        this.lon = lon;
    }

}
