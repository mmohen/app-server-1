package com.ra.rads.kafka.models;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@ToString(exclude = {"id"})
@Document(collection = "flightobjects")
public class FlightObject {

    @Id private String id;
    private String ref;
    private String status;
    private List<FlightObjectRecord> flightobjectrecord;

    public FlightObject(
        final String ref,
        final String status,
        final List<FlightObjectRecord> flightobjectrecord) {
     this.ref = ref;
     this.status = status;
     this.flightobjectrecord = flightobjectrecord;
    }

}
