package com.ra.rads.kafka.models;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;

import java.util.Date;

@Getter
@Setter
@ToString
public class FlightObjectRecord {

    private String app;
    private String status;
    private String airline;
    private String origin;
    private String lon;
    private String lat;
    private String aircrafttype;
    private String departuretime;
    private String arrivaltime;
    private String altitude;
    private String destination;
    private String ref;
    private Date createdAt;

    public FlightObjectRecord(
            final String app,
            final String status,
            final String airline,
            final String origin,
            final String lon,
            final String lat,
            final String aircrafttype,
            final String departuretime,
            final String arrivaltime,
            final String altitude,
            final String destination,
            final String ref) {
        this.app = app;
        this.status = status;
        this.airline = airline;
        this.origin = origin;
        this.lon = lon;
        this.lat = lat;
        this.aircrafttype = aircrafttype;
        this.departuretime = departuretime;
        this.arrivaltime = arrivaltime;
        this.altitude = altitude;
        this.destination = destination;
        this.ref = ref;
    }

}
