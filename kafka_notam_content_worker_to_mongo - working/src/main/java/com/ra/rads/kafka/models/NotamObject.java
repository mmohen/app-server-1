package com.ra.rads.kafka.models;

import com.sun.org.apache.xpath.internal.operations.Bool;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@ToString(exclude = {"id"})
@Document(collection = "notams")
public class NotamObject {

    @Id private String id;
    private String type;
    private String icao;
    private String lat;
    private String lon;
    private Boolean ils_risk;
    private Boolean rwy_risk;
    private org.json.JSONObject body;

    public NotamObject(
            final String type,
            final String icao,
            final String lat,
            final String lon,
            final Boolean ils_risk,
            final Boolean rwy_risk,
            final org.json.JSONObject body) {
        this.type = type;
        this.icao = icao;
        this.ils_risk = ils_risk;
        this.rwy_risk = rwy_risk;
        this.lat = lat;
        this.lon = lon;
        this.body = body;
    }

}
