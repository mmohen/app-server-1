package com.ra.rads.kafka.repositories;

import com.ra.rads.kafka.models.Airport;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface AirportRepository extends MongoRepository<Airport, String> {

    Airport findByIcao(String icao);

}
