package com.ra.rads.kafka.repositories;

import com.ra.rads.kafka.models.FlightObject;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

public interface FlightObjectRepository extends MongoRepository<FlightObject, String> {

  FlightObject findByRef(String ref);

}
