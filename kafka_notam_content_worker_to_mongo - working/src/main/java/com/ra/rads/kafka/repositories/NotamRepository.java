package com.ra.rads.kafka.repositories;

import com.ra.rads.kafka.models.NotamObject;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface NotamRepository extends MongoRepository<NotamObject, String> {


}
