package com.ra.processor;

import com.mongodb.*;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.Date;

public class Mongo {

    public void insertRADSSTT(DBObject input) {

        //System.out.println(input);

        MongoClient mongoClient = null;
        MongoCredential mongoCredential = MongoCredential.createScramSha1Credential("feed", "rads",
                "feed".toCharArray());

        mongoClient = new MongoClient(new ServerAddress("52.1.224.47", 27017), Arrays.asList(mongoCredential));

        DB db = mongoClient.getDB("rads");

        DBCollection table = db.getCollection("notams_new");
        BasicDBObject document = new BasicDBObject();
        document.put("app", "NOTAMS");
        document.put("createdDate", new Date());
        document.put("results", input);
        table.insert(document);

        mongoClient.close();
    }

}
