package com.ra.processor;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource("classpath:ra.properties")
@ConfigurationProperties
public class RAProperties {
	
	private String base_folder;
	private String com_folder;
	private String strip_folder;
	private String uncom_folder;
	private String clean_folder;
	private String processed_folder;
	private String final_folder;
	private String xml_folder;
	private String json_folder;
	private String raw_folder;
	private String mode;
	private String tmp_folder;
	
	public String getTmp_folder() {
		return tmp_folder;
	}
	public void setTmp_folder(String tmp_folder) {
		this.tmp_folder = tmp_folder;
	}
	public String getMode() {
		return mode;
	}
	public void setMode(String mode) {
		this.mode = mode;
	}
	
	public String getStrip_folder() {
		return strip_folder;
	}
	public void setStrip_folder(String strip_folder) {
		this.strip_folder = strip_folder;
	}
	public String getUncom_folder() {
		return uncom_folder;
	}
	public void setUncom_folder(String uncom_folder) {
		this.uncom_folder = uncom_folder;
	}
	public String getClean_folder() {
		return clean_folder;
	}
	public void setClean_folder(String clean_folder) {
		this.clean_folder = clean_folder;
	}
	public String getProcessed_folder() {
		return processed_folder;
	}
	public void setProcessed_folder(String processed_folder) {
		this.processed_folder = processed_folder;
	}
	public String getFinal_folder() {
		return final_folder;
	}
	public void setFinal_folder(String final_folder) {
		this.final_folder = final_folder;
	}
	public String getXml_folder() {
		return xml_folder;
	}
	public void setXml_folder(String xml_folder) {
		this.xml_folder = xml_folder;
	}
	public String getJson_folder() {
		return json_folder;
	}
	public void setJson_folder(String json_folder) {
		this.json_folder = json_folder;
	}
	public String getRaw_folder() {
		return raw_folder;
	}
	public void setRaw_folder(String raw_folder) {
		this.raw_folder = raw_folder;
	}
	public String getBase_folder() {
		return base_folder;
	}
	public void setBase_folder(String base_folder) {
		this.base_folder = base_folder;
	}
	public String getCom_folder() {
		return com_folder;
	}
	public void setCom_folder(String com_folder) {
		this.com_folder = com_folder;
	}
	
	@Override
	public String toString() {
		return "RAProperties [base_folder=" + base_folder + ", com_folder=" + com_folder + ", strip_folder="
				+ strip_folder + ", uncom_folder=" + uncom_folder + ", clean_folder=" + clean_folder
				+ ", processed_folder=" + processed_folder + ", final_folder=" + final_folder + ", xml_folder="
				+ xml_folder + ", json_folder=" + json_folder + ", raw_folder=" + raw_folder + ", mode=" + mode + "]";
	}
	
}
