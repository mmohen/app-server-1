//____        __               __ 
/// __ \____  / /_  __  _______/ /_
/// /_/ / __ \/ __ \/ / / / ___/ __/
/// _, _/ /_/ / /_/ / /_/ (__  ) /_  
///_/ |_|\____/_.___/\__,_/____/\__/  
//  
// RA ASD Processor
// @Author MT Mohen
// April 2017
//
//
//

package com.ra.processor;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.logging.Level;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.transform.Transformer;

import com.mongodb.*;
import com.mongodb.client.MongoClient;
//import jdk.nashorn.internal.parser.JSONParser;
import com.mongodb.util.JSON;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;

import java.io.IOException;

import java.io.InputStreamReader;

import org.apache.http.HttpResponse;

import org.apache.http.client.ClientProtocolException;

import org.apache.http.client.HttpClient;

import org.apache.http.client.methods.HttpGet;

import org.apache.http.impl.client.DefaultHttpClient;


@Component
public class ScheduledTasks {

	@Autowired
	private Environment env;

	private RAProperties global;

	@Autowired
	public void setGlobal(RAProperties global) {
		this.global = global;
	}

	private static final Logger log = LoggerFactory.getLogger(ScheduledTasks.class);
	private static final SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");

	@Scheduled(fixedRate = 1800000)
	public void reportCurrentTime() throws Exception {

		log.info(global.toString());

		log.info("The time is now {}", dateFormat.format(new Date()));

		System.out.println("**************** Start - RA NOTAM Archive **************");

        JSONArray ja = new JSONArray();
        final String url =
                "https://pilotweb.nas.faa.gov/PilotWeb/notamRetrievalByICAOAction.do?method=displayByICAOs&reportType=RAW&formatType=DOMESTIC&retrieveLocId=KATL&actionType=notamRetrievalByICAOs";
        try {
            final Document document = Jsoup.connect(url).get();
            Elements ele = document.select("div#resultsHomeCont").select("div#resultsHomeLeft");
            System.out.println(ele.size());
            for (Element element : ele.select("div#resultsHomeLeft")) {
                String title=element.select("div#resultsHomeLeft").select("div#notamRight").text();
                //System.out.println(title);
                if (title.contains("ATL") && title.contains("CLSD") && title.contains("RWY")) {
                    if(title.length() <= 54) {
                        System.out.println(title);

                        JSONObject jo = new JSONObject();
                        jo.put("app", "LIVE_NOTAMS");
                        jo.put("notam", title);
                        //ja.put(jo);

                        System.out.println(jo.toString());

                        DBObject dbObject = (DBObject) JSON.parse(jo.toString());

                        System.out.println("Inserting into RADS ...");
                        Mongo rads = new Mongo();
                        rads.insertRADSSTT(dbObject);


                    }
                }
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        //System.out.println(ja.toString());



		System.out.println("**************** End - RA NOTAM Archive **************");
	}
}
